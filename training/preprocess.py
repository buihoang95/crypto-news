import re
import pickle

import nltk
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from sklearn.preprocessing import MultiLabelBinarizer

from logger.log import get_logger
try:
    stopwords.words('english')
except Exception as _:
    nltk.download('stopwords')

log = get_logger("preprocess")

stopwords = list(set(stopwords.words('english')))
category_map = {
    "business": "business",
    "cryptocurrency": "blockchain",
    "nft": "nft",
    "bitcoin": "bitcoin",
    "ethereum": "ethereum",
    "technology": "technology",
    "finance": "business",
    "markets": "market",
    "cryptocurrencies": "blockchain",
    "fraud": "policy",
    "scams": "policy",
    "cybercrime": "policy",
    "stocks": "market",
    "altcoin": "altcoin",
    "markets": "market",
    "ether": "ethereum",
    "regulation": "policy",
    "nfts": "nft",
    "crypto": "blockchain",
    "taxes": "policy",
    "defi": "defi",
    "blockchain": "blockchain",
    "blockchains": "blockchain",
    "sec": "policy",
    "coin": "blockchain",
    "btc": "bitcoin",
    "eth": "ethereum",
}
altcoins = ['ADA', 'TrueUSD', 'BNT', 'FUNToken', 'Mdex', 'ALPHA', 'CELO', 'Reef', 'NMR', 'Amp', 'VGX', 'Balancer', 'DFI.Money', 'PHA', 'WAXP', 'LTC', 'PAX', 'WRX',
 'Ethereum Classic', 'HBAR', 'MVL', 'STMX', 'Algorand', 'Ontology Gas', 'GT', 'HUSD', 'LEO', 'Civic', 'Polygon', 'OMG Network', 'Mina', 'NEXO', 'Fantom', 
 'Enjin Coin', 'Bitcoin Standard Hashrate Token', 'SKL', 'NEO', 'LINK', 'ZKSwap', 'REN', 'Bitcoin BEP2', 'CFX', 'Hive', 'AGIX', 'Synthetix', 'FTT', 'UMA',
  'WIN', 'Ardor', 'FTX Token', 'Wootrade', 'REV', 'BTG', 'BTCST', '0x', 'Stellar', 'FIL', 'Klaytn', 'Basic Attention Token', 'BAKE', 'TFUEL', 'XRP', 
  'MaidSafeCoin', 'KCS', 'IOTA', 'TEL', 'OMG', 'ZIL', 'The Graph', 'Casper', 'LSK', 'Crypto.com Coin', 'Perpetual Protocol', 'KSM', 'Golem', 'Curve DAO Token', 
'Kava.io', 'Verge', 'MATIC', 'BakeryToken', 'SHIB', 'Unibright', 'TomoChain', 'META', 'yearn.finance', 'ICP', 'MyNeighborAlice', 'XinFin Network',
 'Celo', 'FTM', 'NEAR', 'ZEN', 'MIOTA', 'Nexo', 'SAND', 'WOO', 'BADGER', 'XLM', 'CAKE', 'BTCB', 'CEL', 'ERG', 'HNT', 'Fetch.ai', 'Smooth Love Potion', 
'CTSI', 'SKALE Network', 'Dai', 'SushiSwap', 'EGLD', 'PAXG', 'PAX Gold', 'ETC', 'Internet Computer', 'PROM', 'Status', 'BitTorrent', 'Bitcoin Cash', 
'Aragon', 'INJ', 'GLM', 'IOTX', 'Celsius', 'Decred', 'DAG', 'SingularityNET', 'WINkLink', 'FLOW', 'Ocean Protocol', 'Siacoin', 'WAX', 'CKB', 'MKR', 
'HIVE', 'ZKS', 'Ren', 'QNT', 'KuCoin Token', 'Horizen', 'Solana', 'BTMX', 'Telcoin', 'OCEAN', 'renBTC', 'BitShares', 'HT', 'Strike', 'UNI', 'VET', 
'Tezos', 'STRAX', 'THETA', 'Cardano', 'SwissBorg', 'POLY', 'DODO', 'Zilliqa', 'NANO', 'NEM', 'NU', 'Ankr', 'Storj', 'USDN', 'Theta Fuel', 'COMP', 'XMR', 
'TerraUSD', 'Constellation', 'Conflux Network', 'Nano', 'Decentraland', 'TUSD', 'AR', 'RSR', 'Arweave', 'DGB', 'Avalanche', 'TOMO', 'UQC', 'Metal',
 'STRK', 'Monero', 'LPT', 'DASH', 'Venus', 'SNT', 'GateToken', 'FUN', 'LRC', 'Zcash', 'Elrond', 'AVAX', 'Paxos Standard', 'Terra', 'Aave', 'Band Protocol',
 'CSPR', 'STX', 'Ravencoin', 'Waves', 'MANA', 'Cartesi', 'SC', 'WAVES', 'UNUS SED LEO', 'Reserve Rights', 'Hedera Hashgraph', 'KLAY', 'Binance Coin', 
'GRT', 'ZRX', 'DAI', 'Chiliz', 'The Sandbox', 'SLP', 'Bitcoin SV', 'VTHO', 'ALGO', 'XTZ', 'Polymath', 'ATOM', 'CRO', 'Loopring', 'MLN', 'Harmony', 
'Orchid', 'WazirX', 'StormX', 'STORJ', 'ANT', 'MAID', 'RLC', 'Bancor', 'YFII', 'Filecoin', 'Helium', 'OKB', 'Polkadot', 'Ergo', 'DOGE', 'BAND',
 'Gemini Dollar', 'FET', 'Quant', 'AMP', 'Serum', 'SNX', 'Orbs', 'ENJ', 'GUSD', 'ASD', 'Cosmos', 'ONG', 'MediBloc', 'ONT', 'Flow', 'GNO', 'PancakeSwap', 'DCR', 
'Stacks', 'USD Coin', 'Augur', 'CELR', 'OGN', 'Huobi Token', 'OXT', 'USDC', 'MTL', 'RIF', 'BAL', 'ARK', 'Steem', 'Kusama', 'Neo', 'TRX', 'DigiByte', 
'MINA', 'Dent', 'Wrapped Bitcoin', 'BTS', 'MCO', 'ARDR', 'BSV', 'NEAR Protocol', 'AAVE', 'ALICE', 'Nervos Network', 'Binance USD', 'SOL', 'Celer Network',
 'Axie Infinity', 'Stratis', 'REEF', 'Metadium', 'SUSHI', 'Badger DAO', 'YFI', 'Enzyme', 'TRON', 'BORA', 'IoTeX', 'ANKR', 'Origin Protocol', 'ONE', 'BTT',
 'RUNE', 'BCD', 'Ontology', 'KAVA', 'ZEC', 'Energy Web Token', 'BAT', 'LUNA', 'iExec RLC', 'Injective Protocol', 'Litecoin', 'NuCypher', 'CRV', 'SHIBAINU',
 'Voyager Token', 'Lisk', 'Neutrino USD', 'BUSD', 'XVG', 'Uniswap', 'Ark', 'VeChain', 'Bitcoin Gold', 'XEM', 'IOST', 'EWT', 'MDX', 'Revain', 'UST',
 'Dash', 'XDC', 'Gnosis', 'AXS', 'NKN', 'SXP', 'BCH', 'CVC', 'Dogecoin', 'XVS', 'THORChain', 'Numeraire', 'BNB', '1INCH', 'WBTC', 'Alpha Finance Lab',
 'SRM', 'Uquid Coin', 'Chainlink', 'Swipe', 'Bitcoin Diamond', 'ICON', 'Prometeus', 'Phala Network', 'DOT', 'STEEM', 'ORBS', 'Compound', 'MED', '1inch',
 'Holo', 'QTUM', 'PERP', 'RSK Infrastructure Framework', 'Livepeer', 'CHZ', 'VeThor Token', 'RENBTC', 'EOS', 'DENT', 'UBT', 'Maker', 'HOT', 'REP', 'Qtum',
 'ICX', 'RVN', 'CHSB']


class TextTransformer():
    def __init__(self, max_len=400, max_words=500):
        self.max_len = max_len
        self.max_words = max_words
        self.tokenizer = None
        
    def transform(self, texts):
        tokenizer = Tokenizer(num_words=self.max_words, lower=True)
        tokenizer.fit_on_texts(texts)
        self.tokenizer = tokenizer
        
        X = tokenizer.texts_to_sequences(texts)
        return pad_sequences(X, maxlen=self.max_len)
    
    def save(self, file_name):
        pickle.dump(self, open(file_name, 'wb'))
        
class MultiLabelTransformer():
    """
    transform_input transform labels array into vector with length is number of unique label in dataset. For example:
    ["blockchain", "nft", "altcoin"] -> [0, 0, 1, 0, 1, 1, 0, 0]
    Args:
        labels
    """
    
    def __init__(self, labels):
        self.labels = labels
        self.multilabel_binarizer = MultiLabelBinarizer()
        self.multilabel_binarizer.fit(labels)
        
    def get_transformed_label(self):
        return self.multilabel_binarizer.transform(self.labels)
    
    def get_number_of_class(self):
        return len(self.multilabel_binarizer.classes_)
    
    def get_label_map(self):
        label_id_to_text = {}
        for i, label in enumerate(self.multilabel_binarizer.classes_):
            label_id_to_text[i] = label
        return label_id_to_text

    def save(self, file_name):
        pickle.dump(self, open(file_name, 'wb'))

class TextCleanner():
    def clean(self, sentence):
        sentence = sentence.lower()
        sentence = sentence.replace("..", ".")
        sentence = sentence.replace(". ", " . ")
        sentence = sentence.replace(", ", " , ")
        sentence = sentence.strip("/ . , ; / ( ) / { }[\" ] # $ @ ! % ^ & * +/-\ _ /")
        return sentence

    def decontract(self, sentence):
        sentence = re.sub(r"n\'t", " not", sentence)
        sentence = re.sub(r"\'re", " are", sentence)
        sentence = re.sub(r"\'s", " is", sentence)
        sentence = re.sub(r"\'d", " would", sentence)
        sentence = re.sub(r"\'ll", " will", sentence)
        sentence = re.sub(r"\'t", " not", sentence)
        sentence = re.sub(r"\'ve", " have", sentence)
        sentence = re.sub(r"\'m", " am", sentence)
        return sentence

    def remove_stopwords(self, sentence):
        sentence = ' '.join([word for word in sentence.split() if word not in stopwords])
        return sentence

    def preprocess(self, sentence):
        sentence = self.decontract(sentence)
        sentence = self.remove_stopwords(sentence)
        sentence = self.clean(sentence)
        return sentence
    
class LabelMaker():
    def _find_labels(self, text):
        res = []    
        for altcoin in altcoins:
            if altcoin in text:
                res.append("altcoin")
                
        text = text.lower()
        for k in category_map:
            if k in text.split():
                if category_map[k] == "blockchain":
                    continue
                
                res.append(category_map[k])
            
        return list(set(res))
    
    def make_labels(self, df):
        df["labels_in_content"] = df["titles"].map(self._find_labels)
        df["labels_in_tag"] = df["categories"].map(self._find_labels)
        
        df["labels"] = df["labels_in_content"] + df["labels_in_tag"]
        df["labels"] = df["labels"].map(lambda x: list(set(x)))
        
        return df