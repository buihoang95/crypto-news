from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Activation, Embedding, Flatten, GlobalMaxPool1D, Dropout, Conv1D
from tensorflow.keras.layers import Flatten, LSTM
from tensorflow.keras.models import Model
from tensorflow.keras.callbacks import ReduceLROnPlateau, EarlyStopping, ModelCheckpoint
from tensorflow.keras.losses import binary_crossentropy
from tensorflow.keras.optimizers import Adam

class LSTMModel():
    def __init__(self, num_classes,
                embedding_length,
                embedding_maxword,
                embedding_dim=20,
                unit=32,
                dropout=0.2,
                activation="sigmoid"):

        self.num_classes = num_classes
        self.embedding_length = embedding_length
        self.embedding_maxword = embedding_maxword
        self.embedding_dim = embedding_dim
        self.unit = unit
        self.dropout = dropout
        self.activation = activation
        self.model = None
    
    def create_model(self):
        self.model = Sequential()
        self.model.add(Embedding(self.embedding_maxword, self.embedding_dim, input_length=self.embedding_length))
        self.model.add(Dropout(self.dropout))
        self.model.add(LSTM(self.unit, dropout=0.3))
        self.model.add(Dense(self.num_classes))
        self.model.add(Activation(self.activation))

        self.model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['categorical_accuracy'])
        self.model.summary()
    
    def train(self,
              X_train, y_train,
              epoch=50, batch_size=16):
        if self.model is None:
            raise ValueError("Need construct model via `create_model` before train")
        
        callbacks = [
            ReduceLROnPlateau(), 
            EarlyStopping(patience=4), 
            ModelCheckpoint(filepath='model/model-lstm.h5', save_best_only=True)
        ]
        self.model.fit(X_train, y_train, epochs=epoch, batch_size=batch_size, validation_split=0.1, callbacks=callbacks)
        
    def evaluate(self, X_test, y_test):
        metrics = self.model.evaluate(X_test, y_test)
        print("{}: {}".format(self.model.metrics_names[0], metrics[0]))
        print("{}: {}".format(self.model.metrics_names[1], metrics[1]))
        
    def predict(self, X):
        return self.model.predict(X)