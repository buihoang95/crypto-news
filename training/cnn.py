import tensorflow as tf
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import Dense, Activation, Embedding, Flatten, GlobalMaxPool1D, Dropout, Conv1D
from tensorflow.keras.layers import Concatenate, Flatten, Dense, Input
from tensorflow.keras.callbacks import ReduceLROnPlateau, EarlyStopping, ModelCheckpoint
from tensorflow.keras.losses import binary_crossentropy
from tensorflow.keras.optimizers import Adam

class CNN():
    def __init__(self, num_classes,
                embedding_length,
                embedding_maxword,
                filter_size=200,
                embedding_dim=20,
                dropout=0.2,
                activation="sigmoid"):

        self.num_classes = num_classes
        self.embedding_length = embedding_length
        self.embedding_maxword = embedding_maxword
        self.filter_size = filter_size
        self.embedding_dim = embedding_dim
        self.dropout = dropout
        self.activation = activation
        self.model = None
    
    def create_model(self):
        self.input_shape = Input(shape=(self.embedding_length))
        self.embedding = Embedding(self.embedding_maxword, self.embedding_dim, input_length=self.embedding_length)(self.input_shape)
        self.dropout_layer = Dropout(self.dropout)(self.embedding)
        
        kernels = [3,4,5]
        pool_layers = []
        for kernel in kernels:
            conv_layer = Conv1D(self.filter_size, kernel, padding='same', activation='relu', strides=1)(self.dropout_layer)
            pool_layer = GlobalMaxPool1D()(conv_layer)
            pool_layers.append(pool_layer)
        
        self.merged = tf.keras.layers.concatenate(pool_layers, axis=1)
        self.merged = Flatten()(self.merged)
        self.dropout_layer_2 = Dropout(self.dropout)(self.merged)
        
        self.out_1 = Dense(50, activation='relu')(self.dropout_layer_2)
        self.out = Dense(self.num_classes, activation='sigmoid')(self.out_1)

        self.model = Model(self.input_shape, self.out)
        self.model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['categorical_accuracy'])
        self.model.summary()
        
    def create_model2(self):
        self.model = Sequential()
        self.model.add(Embedding(self.embedding_maxword, self.embedding_dim, input_length=self.embedding_length))
        self.model.add(Dropout(self.dropout))
        self.model.add(Conv1D(self.filter_size, 3, padding='same', activation='relu', strides=1))
        self.model.add(GlobalMaxPool1D())
        self.model.add(Dense(self.num_classes))
        self.model.add(Activation(self.activation))

        self.model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['categorical_accuracy'])
        self.model.summary()

    
    def train(self,
              X_train, y_train,
              epoch=50, batch_size=16):
        if self.model is None:
            raise ValueError("Need construct model via `create_model` before train")
        
        callbacks = [
            ReduceLROnPlateau(), 
            EarlyStopping(patience=4), 
            ModelCheckpoint(filepath='model/model-conv1d.h5', save_best_only=True)
        ]
        self.model.fit(X_train, y_train, epochs=epoch, batch_size=batch_size, validation_split=0.1, callbacks=callbacks)
        
    def evaluate(self, X_test, y_test):
        metrics = self.model.evaluate(X_test, y_test)
        print("{}: {}".format(self.model.metrics_names[0], metrics[0]))
        print("{}: {}".format(self.model.metrics_names[1], metrics[1]))
        
    def predict(self, X):
        return self.model.predict(X)