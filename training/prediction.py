import pickle
import os
import sys
import json
import argparse
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))


import pandas as pd
import tensorflow as tf

from training.preprocess import TextCleanner
from logger.log import get_logger

log = get_logger("prediction")

def predict_prob(prob):
    if prob > 0.5:
        return 1
    return 0

label_maker = pickle.load(open("model/label_transformer.pickle", 'rb'))
text_transformer = pickle.load(open("model/tex_transformer.pickle", 'rb'))
cnn_model = tf.keras.models.load_model('model/model-conv1d.h5')
label_id_to_text = label_maker.get_label_map()

def predict(input_file, output_file):
    log.info("Start training")
    log.info("Reading dataset")
    df = pd.read_csv(input_file)
    
    df = df[["urls", "titles", "contents"]]
    df = df.dropna()
    
    log.info("Cleaning the dataset")
    # concat title and content, and clean
    df["texts"] = df["titles"] + " . " + df["contents"]
    text_cleaner = TextCleanner()
    df["texts"] = df["texts"].map(text_cleaner.preprocess)   
    
    # convert label to muti-label array
    
    log.info("Transforming the dataset")
    # transform text to vector
    X = text_transformer.transform(df["texts"])
    predictions = cnn_model.predict(X)
    
    for index,label in label_id_to_text.items():
        prediction_label = predictions[:,index]
        prediction_label = list(map(predict_prob, prediction_label))
        df[label] = prediction_label
        
    result = {}
    for label in label_id_to_text.values():
        urls = df[df[label] == 1].urls.values.tolist()
        result[label] = urls
    result["blockchain"] = df.urls.values.tolist()
    
    with open(output_file, 'w') as fp:
        json.dump(result, fp)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_file", type=str, default="data/post.csv",
                        help="input file, contain `urls` column")

    parser.add_argument("--output_file", type=str, default="data/labels.json",
                        help="output file")
    
    FLAGS = parser.parse_args()
    predict(FLAGS.input_file, FLAGS.output_file)