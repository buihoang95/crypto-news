import os
import sys
import argparse
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

import pandas as pd
import tensorflow as tf
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.model_selection import train_test_split

from training.preprocess import TextTransformer, MultiLabelTransformer, TextCleanner, LabelMaker
from training.cnn import CNN
from training.lstm import LSTMModel
from logger.log import get_logger

log = get_logger("runner")


def train(model_name):
    log.info("Start training")
    log.info("Reading dataset")
    post_df = pd.read_csv("data/post.csv")
    external_df = pd.read_csv("data/entrepreneur_crawled.csv")
    
    post_df = post_df.dropna()
    external_df = external_df.dropna()
    
    label_maker = LabelMaker()
    post_df = label_maker.make_labels(post_df)
    external_df = label_maker.make_labels(external_df)
    
    df = pd.concat([post_df, external_df])
    
    log.info("Cleaning the dataset")
    # concat title and content, and clean
    df["texts"] = df["titles"] + " . " + df["contents"]
    text_cleaner = TextCleanner()
    df["texts"] = df["texts"].map(text_cleaner.preprocess)   
    
    # convert label to muti-label array
    
    log.info("Transforming the dataset")
    # transform text to vector
    max_len = 400
    max_words = 300
    text_transformer = TextTransformer(max_len=max_len, max_words=max_words)
    X = text_transformer.transform(df["texts"])
    
    # transform label to vector
    label_transformer = MultiLabelTransformer(df["labels"])
    y = label_transformer.get_transformed_label()
    
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
    
    num_classes = label_transformer.get_number_of_class()
    
    if model_name not in ["lstm", "cnn"]:
        raise ValueError("model must be `lstm` or `cnn`")
    
    if model_name == "lstm":
        log.info("Training the LSTM model")
        model = LSTMModel(num_classes=num_classes,
                              embedding_length=max_len,
                              embedding_maxword= max_words,
                              unit=128)
        
        model.create_model()
        model.train(X_train, y_train)
        model.evaluate(X_test, y_test)
    
    if model_name == "cnn":
        log.info("Training the CNN model")
        model = CNN(num_classes=num_classes, embedding_length=max_len, embedding_maxword= max_words)
        model.create_model()
        model.train(X_train, y_train)
        model.evaluate(X_test, y_test)

    text_transformer.save("model/tex_transformer.pickle")
    label_transformer.save("model/label_transformer.pickle")
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--model", type=str, default="cnn",
                        help="model type")
    
    FLAGS = parser.parse_args()
    train(FLAGS.model)