import time
import os
import sys
import csv
import argparse
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

import requests
from bs4 import BeautifulSoup
import pandas as pd
import brotli

from logger.log import get_logger

log = get_logger("crawler")

css_title = "title"

css_contents_level_1 = [
    'div.art-v2-body',
    'div.ezrichtext-field',
    'div.content-inner',
    'div.m-detail--body',
    'div.post-content',
    'div.space-page-content-wrap',
    'div.entry-content',
    "div.article-content-wrapper",
    "div.post-box",
    "div.caas-content-wrapper"
    "div.gutenbergContent__content--1FgGp",
    "article.article__body",
    "div.body",
    "div.paywall",
    "div.article-body",
    "div.article__content-body"
]
css_contents_level_2 = ['p', 'ol', "h1", "h2", "h3", "li"]
categories_css = [
    "div.tagcloud",
    "ul.tags-list__list",
    "div.posted-in",
    "div.article__body__tags",
    "span.entry-tags",
    "span.tagcloud",
    "div.entry-tags"]

headers = {
    "cointelegraph.com": {
        "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "accept-language": "en-US,en;q=0.9,vi;q=0.8",
        "cache-control": "max-age=0",
        "cookie": "SessionGA=9ba50049472bcc2ebb78450a52b5e2b9; auth=no; __auc=9f885b8217b1a85c8290b9c5d07; _cb_ls=1; _cb=CNbqjhCMBVFFDz4tki; _hjid=4a93ad16-398f-4af2-8c3b-9e6013508d15; _fbp=fb.1.1628237580517.1267863850; _chartbeat2=.1628237580403.1628509408547.1001.B0NXw7DNQn2PBTqB1LJkizWkoLSn.1; cointelegraph_com_session=eyJpdiI6InN1ekE5YkVhVHVXRmdvR2phXC9IUzF3PT0iLCJ2YWx1ZSI6IktDajVBN0JtYllxTlpUN0xnZ2lFZyt6Nk1wQzYwdWc2NGJ1dUh0UkQrTVdZWGgzREcxRkpDcGE3amoyOW5pUm8iLCJtYWMiOiJkZDIwZjBkMjViNWJkNjNjNTlhODc5YmIyNjZlYmI3NGNjMTlkNDk1OTNiYWZjY2Q3MDg5NzY2ZjZlYzBkYmJjIn0%3D; _gid=GA1.2.400237499.1628509409; _ga=GA1.2.1579635080.1628237581; _gat_UA-45656735-1=1; _ga_0GH804GDEW=GS1.1.1628520104.3.0.1628520104.60",
        "sec-ch-ua": 'Chromium";v="92", " Not A;Brand";v="99", "Google Chrome";v="92"',
        "sec-ch-ua-mobile": "?0",
        "sec-fetch-dest": "document",
        "sec-fetch-mode": "navigate",
        "sec-fetch-site": "same-origin",
        "sec-fetch-user": "?1",
        "upgrade-insecure-requests": "1",
        "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.131 Safari/537.36",
    }
}
encoded_urls = [
    "unchainedpodcast.com"
]
categories_meta_css = [
    "entrepreneur.com"
]


class Crawler():
    def crawl_static_sites(self, url):
        try:
            domain = url.split('/')[2].replace("www.", "")
            
            url_header = None
            if headers.get(domain, None) is not None:
                url_header = headers.get(domain, None)
                
            res = requests.get(url, headers=url_header, timeout=10)
            
            content = res.text
            if domain in encoded_urls:
                content = brotli.decompress(res.content)
            
            soup = BeautifulSoup(content, "lxml")
            
            # get title
            title_elem = soup.select_one(css_title)
            title = title_elem.text.strip()
            
            # get content
            content = None
            for content_css in css_contents_level_1:
                content_elem = soup.select_one(content_css)
                if content_elem is None:
                    continue

                content_elem = content_elem.find_all(css_contents_level_2)
                content = ". ".join([elem.text.strip() for elem in content_elem])
            
            # get categories
            categories = None
            if domain in categories_meta_css:
                categories = soup.find("meta", {"property": "schema:keywords"}).attrs['content']
            for css in categories_css:
                category_elem = soup.select_one(css)
                if category_elem is None:
                    continue
                
                categories = category_elem.text.strip()
            return title, content, categories
        except Exception as e:
            log.info("----------------------")
            log.info(url)
            log.info(e)
            return None, None, None
        
    def crawl_medium(self, url):
        try:
            res = requests.get(url, timeout=5)
            soup = BeautifulSoup(res.text, "lxml")
            
            title_elem = soup.select_one(css_title)
            title = title_elem.text.strip()
            
            content_elem = soup.findAll(css_contents_level_2)
            if content_elem is None:
                return
            content = ". ".join([elem.text.strip() for elem in content_elem])
            return title, content, None
        except Exception as e:
            log.info("----------------------")
            log.info(url)
            log.info(e)
            return None, None, None
    
    def crawl(self, url):
        domain = url.split('/')[2].replace("www.", "")
        
        if domain == "medium.com":
            return self.crawl_medium(url)
        
        return self.crawl_static_sites(url)

def run(input_file, output_file):
    df = pd.read_csv(input_file)
    # df["domains"] = df.urls.map(lambda url: url.split('/')[2].replace("www.", ""))
    # df = df.groupby("domains").agg("max")
    
    news_crawler = Crawler()
    log.info(f"Start crawling, read from file {input_file}, output to file {output_file}")
    
    with open(output_file, 'w') as f:
        # create the csv writer
        writer = csv.writer(f)
        writer.writerow(["urls", "titles", "contents", "categories"])
        
        for url in df["urls"].values:
            log.info(url)
            title, content, categories = news_crawler.crawl(url)
            if content is not None:
                content = content.replace("\n", " . ")
                
            if categories is not None:
                categories = categories.replace("\n", " . ")
            writer.writerow([url, title, content, categories])


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_file", type=str, default="data/urls.csv",
                        help="input file, contain `urls` column")

    parser.add_argument("--output_file", type=str, default="data/entrepreneur_crawled.csv",
                        help="output file")
    
    FLAGS = parser.parse_args()
    run(FLAGS.input_file, FLAGS.output_file)
    