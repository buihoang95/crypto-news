# CRYPTO NEWS CLASSIFICATION
## I. Overview
This project contain 2 main functions:
- crawl news by given urls in **data/urls.csv** file
- Use training data in **data/training_data.csv** to create **Multi-Label Classification Model**, which classify news posts to categories
  
## II. Prerequisites
- Python 3.6 (tested)
- pip3

To install dependence, use command:
> pip3 install -r requirements.txt

## III. How to run
### 1. Crawl news
There are 2 dataset provided
1. data/urls : dataset contain various news from assignment
2. data/entrepreneur.csv dataset contain url of domain (entrepreneur.com), use as external data

- To crawl news from **data/urls.csv** use command:
> python3 crawler/crawler.py --input_file=data/urls.csv --output_file=data/post.csv

where *output=data/post.csv* is data_path where output file located

- To crawl news from **data/entrepreneur.csv** use command:
> python3 crawler/crawler.py --input_file=data/entrepreneur.csv --output_file=data/entrepreneur_crawled.csv

where *output=data/entrepreneur_crawled.csv* is data_path where output file located

### 2. Train model
To train model, we have crawl data from previous step first.
Training use 2 data files: **data/post.csv** and **data/entrepreneur.csv**

use command:
> python3 training/trainer.py --model=cnn

where model is kind of model use to train, at that moments, there are 2 options: **lstm**(Long Short Term Memory) and **cnn**(Convolution Neural Network)

After training, you can see there is the model, and 2 pickle transformation object generated.

### 3. Predict labels
To run prediction, you have to train the model first.
make sure **data/post.csv** is available

Run command:
> python3 training/prediction.py

After complete prediction, you can see file named **data/labels.json**, which contains labels predict from urls

